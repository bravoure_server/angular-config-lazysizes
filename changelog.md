## Angular Config LazySizes / Bravoure component

This Component configs LazySizes in angular.

### **Versions:**

1.0.2   - Updated configuration
1.0.1   - Updated loading media queries
1.0     - Initial Stable version

---------------------
