
if (!window.HTMLPictureElement || !('sizes' in document.createElement('img'))) {
    document.createElement("picture");
    //loadJS("ls.respimg.min.js");
}


/**
 * FF's first picture implementation is static and does not react to viewport changes, this tiny script fixes this.
 */
(function(){
    /*jshint eqnull:true */
    var ua = navigator.userAgent;

    if(window.HTMLPictureElement && ((/ecko/).test(ua) && ua.match(/rv\:(\d+)/) && RegExp.$1 < 41)){
        addEventListener('resize', (function(){
            var timer;

            var dummySrc = document.createElement('source');

            var fixPicture = function(img){
                var picture = img.parentNode;
                var source = dummySrc.cloneNode();
                picture.insertBefore(source, picture.firstElementChild);
                setTimeout(function(){
                    picture.removeChild(source);
                });
            };

            var findPictureImgs = function(){
                var i;
                var imgs = document.querySelectorAll('picture > img');
                for(i = 0; i < imgs.length; i++){
                    if(imgs[i].complete){
                        if(imgs[i].currentSrc){
                            fixPicture(imgs[i]);
                        }
                    } else if(imgs[i].currentSrc){
                        removeEventListener('resize', onResize);
                        break;
                    }
                }
            };
            var onResize = function(){
                clearTimeout(timer);
                timer = setTimeout(findPictureImgs, 99);
            };

            dummySrc.srcset = 'data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==';

            return onResize;
        })());
    }
})();

(function () {
    'use strict';

    function runLazysizes($window) {
        // to init lazysizes at app start

        $window.lazySizesConfig = $window.lazySizesConfig || {};

        $window.lazySizesConfig.preloadClass= 'lazypreload';
        $window.lazySizesConfig.minSize = 40;
        $window.lazySizesConfig.init = true;
        $window.lazySizesConfig.expand = 1200;
        $window.lazySizesConfig.loadMode = 2;
        $window.lazySizesConfig.addClasses = true;
        $window.lazySizesConfig.ricTimeout = 50;

        $window.lazySizesConfig.customMedia = {
            '--small': '(max-width: 767px)',
            '--medium': '(max-width: 991px)',
            '--large': '(max-width: 1199px)',
            '--largest': '(max-width: 3000px)'
        };
    }

    runLazysizes.$inject = ['$window'];

    angular
        .module('bravoureAngularApp')
        .run(runLazysizes);

})();
